const signInScreen = require('../page-objects/sign-in-screen');

describe('Sign In Tests', function() {

    beforeEach(function() {
        browser.get(`${signInScreen.url}`);
    });

    it('should show warnings when signing in with empty input fields', function() {
        signInScreen.signIn('', '');

        expect(signInScreen.elements.emailError.getText()).toEqual('Please enter your email to get started.');
        expect(signInScreen.elements.passwordError.getText()).toEqual('We need a valid password.');

        signInScreen.signIn('       ', '');

        expect(signInScreen.elements.emailError.getText()).toEqual('Please enter your email to get started.');
    });

    it('should show warning when signing in with not valid email', function() {
        signInScreen.signIn('123', 'validPASSWORD');

        expect(signInScreen.elements.emailError.getText()).toEqual('We need a valid email address.');
    });

});