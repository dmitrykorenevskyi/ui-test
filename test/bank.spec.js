const loginScreen = require('../page-objects/bank/login-screen');
const customerMainScreen = require('../page-objects/bank/customer-screen');
const accountCustomerScreen = require('../page-objects/bank/account-screen');
const managerMainScreen = require('../page-objects/bank/manager-screen');
const commonHelpers = require('../page-objects/common-helpers');

describe('Customer ', () => {

    beforeEach(() => {
        browser.get(`${loginScreen.url}`);
        customerMainScreen.signInAsHermiona();
    });

    it('should be possible to logout from account', function() {
        accountCustomerScreen.makeDepositTransaction(100);
    });

});

describe('Customer Deposit and Withdraw ', () => {

    beforeEach(() => {
        browser.get(`${loginScreen.url}`);
        customerMainScreen.signInAsHermiona();
    });

    it('should be able to make Deposit transaction', function() {
        accountCustomerScreen.makeDepositTransaction(100);
        accountCustomerScreen.checkTransactionResultMessage('Deposit Successful');
    });

    it('should be able to make Withdraw transaction', function() {
        accountCustomerScreen.makeWithdrawTransactionAndCheckBalance(100);
        accountCustomerScreen.checkTransactionResultMessage('Transaction successful');
    });

    it('should be impossible to make Withdraw transaction if not not enough money', function() {
        accountCustomerScreen.changeAccount();
        accountCustomerScreen.makeWithdrawTransaction(100);
        accountCustomerScreen.checkTransactionResultMessage('Transaction Failed. ' +
            'You can not withdraw amount more than the balance.');
    });
});

describe('Customer Transactions History: ', function() {

    beforeEach(() => {
        browser.get(`${loginScreen.url}`);
        customerMainScreen.signInAsHermiona();
        accountCustomerScreen.changeAccount();
    });

    it('Deposit transaction should appear on Tx history screen', function() {
        accountCustomerScreen.makeDepositTransaction(100);
        accountCustomerScreen.checkLastTransaction(100, 'Credit');
    });

    it('Withdraw transaction should appear on Tx history screen', function() {
        accountCustomerScreen.makeWithdrawTransaction(100);
        accountCustomerScreen.checkLastTransaction(100, 'Debit');
    });

    it('Reset button should clear Tx history', function() {
        accountCustomerScreen.resetTransactionsListAndCheckThatItCleared();
    });

});

describe('Manager account creation: ', () => {

    beforeEach(() => {
        browser.get(`${loginScreen.url}`);
        loginScreen.goToManagerLoginScreen();
    });

    it('should be able to create new user', function() {
        managerMainScreen.addNewUserAndCheckSuccessAlert('Creation', 'Success', '1111111');
        managerMainScreen.checkThatUserIsCreated('Creation', 'Success', '1111111');
    });

    it('should be impossible to duplicate user', function() {
        managerMainScreen.addNewUserAndCheckSuccessAlert('Duplicate', 'Test', '1111111');
        managerMainScreen.addNewUserAndCheckDuplicateAlert('Duplicate', 'Test', '1111111');
    });

    it('should be impossible use created account without opening', function() {
        managerMainScreen.addNewUserAndCheckSuccessAlert('Closed', 'Account', '1111111');
        commonHelpers.goToHomeScreen();
        customerMainScreen.signInAsLastCreatedUser();
        accountCustomerScreen.checkThatAccountIsClosed();
    });

    it('should be able to open Dollar account for new created user', function() {
        managerMainScreen.addNewUserAndCheckSuccessAlert('Dollar', 'Account', '1111111');
        managerMainScreen.openAccountForLastCreatedUser('Dollar');

        commonHelpers.goToHomeScreen();
        customerMainScreen.signInAsLastCreatedUser();

        accountCustomerScreen.checkThatAccountIsOpenedWithChosenCurrency('Dollar');
    });

    it('should be possible to look number of opened account', function() {
        managerMainScreen.chooseFirstOpenedDollarAccountAndCheckAlert();
    });

    it('should be able to open Pound account for new created user', function() {
        managerMainScreen.addNewUserAndCheckSuccessAlert('Pound', 'Account', '1111111');
        managerMainScreen.openAccountForLastCreatedUser('Pound');

        commonHelpers.goToHomeScreen();
        customerMainScreen.signInAsLastCreatedUser();

        accountCustomerScreen.checkThatAccountIsOpenedWithChosenCurrency('Pound');
    });

    it('should be able to open Rupee account for new created user', function() {
        managerMainScreen.addNewUserAndCheckSuccessAlert('Rupee', 'Account', '1111111');
        managerMainScreen.openAccountForLastCreatedUser('Rupee');

        commonHelpers.goToHomeScreen();
        customerMainScreen.signInAsLastCreatedUser();

        accountCustomerScreen.checkThatAccountIsOpenedWithChosenCurrency('Rupee');
    });
});