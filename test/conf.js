let fs = require('fs-extra');
let jasmineReporters = require('jasmine-reporters');
let htmlReporter = require('../lib/protractor-xml2html-reporter');


exports.config = {

    framework: 'jasmine2',
    specs: ['bank.spec.js'],
    seleniumServerJar: '../node_modules/protractor/node_modules/webdriver-manager/selenium/selenium-server-standalone-3.14.0.jar',
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            args: [ "--headless", "--disable-gpu", "--window-size=1280,720", "--no-sandbox", "--disable-dev-shm-usage" ]
        }
    },

    onPrepare: function() {

        fs.emptyDir('./reports/xml/', function (err) {
            console.log(err);
        });

        fs.emptyDir('./reports/screenshots/', function (err) {
            console.log(err);
        });

        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: './reports/xml/',
            filePrefix: 'xmlresults',
        }));

        jasmine.getEnv().addReporter({
            specDone: function(result) {
                if (result.status === 'failed') {
                    browser.getCapabilities().then(function (caps) {
                        let browserName = caps.get('browserName');

                        browser.takeScreenshot().then(function (png) {
                            let stream = fs.createWriteStream('./reports/screenshots/' + browserName + '-' + result.fullName+ '.png');
                            stream.write(new Buffer(png, 'base64'));
                            stream.end();
                        });
                    });
                }
            }
        });

    },

    onComplete: function() {
        let browserName, browserVersion;
        let capsPromise = browser.getCapabilities();

        capsPromise.then(function (caps) {
            browserName = caps.get('browserName');
            browserVersion = caps.get('version');
            platform = caps.get('platform');

            testConfig = {
                reportTitle: 'Protractor Test Execution Report',
                outputPath: './reports/',
                outputFilename: 'ProtractorTestReport',
                screenshotPath: './screenshots',
                testBrowser: browserName,
                browserVersion: browserVersion,
                modifiedSuiteName: false,
                screenshotsOnlyOnFailure: true,
                testPlatform: platform
            };
            new htmlReporter().from('./reports/xml/xmlresults.xml', testConfig);
        });
    }

};