let EC = protractor.ExpectedConditions;

module.exports = {

    elements: {
        homeButton: $('.home')
    },

    goToHomeScreen () {
        this.elements.homeButton.click();
    },

    checkAlertText(elementToClick, alertText) {
        elementToClick.click().then(null, function(err) {
            browser.wait(EC.alertIsPresent(), 5000);
            browser.switchTo().alert().accept();
            expect(err.message).toContain(alertText);
        });

    }
};