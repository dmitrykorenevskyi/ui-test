module.exports = {
    url: 'https://www.jetblue.com/signin?returnUrl=https:%2F%2Fwww.jetblue.com%2F',

    elements: {
        emailInput: $('[type=email]'),
        emailError: $('#emailError'),
        passwordInput: $('[type=password]'),
        passwordError: $('#passwordError'),
        signInButton: element(by.buttonText('Sign in')),
        forgotPassword: element(by.linkText('Forgot password?'))
    },

    signIn (email, password) {
        this.elements.emailInput.sendKeys(email);
        this.elements.passwordInput.sendKeys(password);
        this.elements.signInButton.click();
    }
};
