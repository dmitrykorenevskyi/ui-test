let commonHelpers = require('../common-helpers');

module.exports = {

    url: 'http://www.way2automation.com/angularjs-protractor/banking/#/',

    elements: {

        addCustomerButton: $('[ng-click="addCust()"]'),
        openAccountButton: $('[ng-click="openAccount()"]'),
        customersButton: $('[ng-click="showCust()"]'),

        submitButton: $('[type=submit]'),

        addCustomer: {
            firstNameInput: element(by.model('fName')),
            lastNameInput: element(by.model('lName')),
            postCodeInput: element(by.model('postCd'))
        },

        openAccount: {
            customerList: element.all(by.repeater('cust in Customers')),

            dollarCurrency: $('[value=Dollar]'),
            poundCurrency: $('[value=Pound]'),
            rupeeCurrency: $('[value=Rupee]')
        },

        customers: {
            list: element.all(by.repeater('cust in Customers')),
        }
    },

    // A D D  C U S T O M E R //

    fillFirstName (firstName) {
        this.elements.addCustomer.firstNameInput.sendKeys(firstName);
    },

    fillLastName (lastName) {
        this.elements.addCustomer.lastNameInput.sendKeys(lastName);
    },

    fillPostCode (postCode) {
        this.elements.addCustomer.postCodeInput.sendKeys(postCode);
    },

    addNewUser (firstName, lastName, postCode) {
        this.elements.addCustomerButton.click();
        this.fillFirstName(firstName);
        this.fillLastName(lastName);
        this.fillPostCode(postCode);
        this.elements.submitButton.click();
    },

    addNewUserAndCheckSuccessAlert (firstName, lastName, postCode) {
        this.addNewUser(firstName, lastName, postCode);
        commonHelpers.checkAlertText(this.elements.addCustomerButton,
            'Customer added successfully with customer id');
    },

    addNewUserAndCheckDuplicateAlert (firstName, lastName, postCode) {
        this.addNewUser(firstName, lastName, postCode);
        commonHelpers.checkAlertText(this.elements.addCustomerButton,
            'Please check the details. Customer may be duplicate.');
    },

    // O P E N  A C C O U N T //

    openAccountForLastCreatedUser (currency) {
        let chosenCurrency;

        switch (currency) {
            case 'Dollar':
                chosenCurrency = this.elements.openAccount.dollarCurrency;
                break;
            case 'Pound':
                chosenCurrency = this.elements.openAccount.poundCurrency;
                break;
            case 'Rupee':
                chosenCurrency = this.elements.openAccount.rupeeCurrency;
                break;
            default:
                alert( 'Wrong chosen currency. Dollar, Pound and Rupee allowed' );
        }

        this.openAccount(chosenCurrency);
        commonHelpers.checkAlertText(this.elements.addCustomerButton, 'Account created successfully with account Number')
    },

    openAccount (currency) {
        this.elements.openAccountButton.click().then(() => {
            this.elements.openAccount.customerList.last().click();
            currency.click();

            this.elements.submitButton.click();
        });
    },

    chooseFirstOpenedDollarAccountAndCheckAlert () {
        this.elements.openAccountButton.click().then(() => {
            this.elements.openAccount.customerList.first().click();
            this.elements.openAccount.dollarCurrency.click();

            this.elements.submitButton.click().then(() => {
                commonHelpers.checkAlertText(this.elements.addCustomerButton, 'Account created successfully ' +
                    'with account Number :')
            });
        });
    },

    // C U S T O M E R S //

    checkThatUserIsCreated (firstName, lastName, postCode) {
        this.elements.customersButton.click();
        expect(this.elements.customers.list.last().getText()).toContain(firstName +' '+ lastName +' '+ postCode);
    }
};