let loginScreen = require('./login-screen');

module.exports = {

    url: 'http://www.way2automation.com/angularjs-protractor/banking/#/customer',

    elements: {
        userSelector: element.all(by.repeater('cust in Customers')),
        loginButton: element(by.buttonText('Login')),
    },

    signInAsHermiona () {
        loginScreen.goToCustomerLoginScreen();
        this.elements.userSelector.first().click();
        this.elements.loginButton.click();
    },

    signInAsLastCreatedUser () {
        loginScreen.goToCustomerLoginScreen();
        this.elements.userSelector.last().click();
        this.elements.loginButton.click();
    }

};