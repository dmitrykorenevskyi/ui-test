loginScreen = require('./login-screen');

module.exports = {

    url: 'http://www.way2automation.com/angularjs-protractor/banking/#/account',

    elements: {
        transactionsButton: element(by.partialButtonText('Transactions')),
        depositButton: element(by.partialButtonText('Deposit')),
        withdrawButton: element(by.partialButtonText('Withdraw')),

        currentAccountNumber: element(by.xpath('/html/body/div[3]/div/div[2]/div/div[2]/strong[1]')),
        currentBalance: element(by.xpath('/html/body/div[3]/div/div[2]/div/div[2]/strong[2]')),
        currentCurrency: element(by.xpath('/html/body/div[3]/div/div[2]/div/div[2]/strong[3]')),

        selectAccount: $('#accountSelect'),
        logoutButton: $('[ng-show=logout]'),

        accountClosedMessage: element(by.xpath('/html/body/div[3]/div/div[2]/div/div[1]/span')),

        //t r a n s a c t i o n s//

        transactionDetails: element.all(by.repeater('tx in transactions')),
        resetButton: $('[ng-click="reset()"]'),

        //c o m m o n//

        amountInputField: $('[placeholder=amount]'),
        resultMessage: $('[ng-show=message]'),
        submitButton: $('[type=submit]'),

    },

    // T R A N S A C T I O N S //

    checkLastTransaction (amount, txType) {
        browser.sleep(1000);
        this.elements.transactionsButton.click().then(() => {
            expect(this.elements.transactionDetails.last().getText()).toContain(amount +' '+ txType);
        });
    },

    resetTransactionsListAndCheckThatItCleared () {
        this.elements.transactionsButton.click().then(() => {
            this.elements.resetButton.click().then(() => {
                expect(this.elements.transactionDetails.first().isPresent()).toBe(false);
            })
        });
    },

    // D E P O S I T //

    makeDepositTransaction (amount) {
        this.elements.currentBalance.getText().then((beforeBalance) => {
            this.elements.depositButton.click().then(() => {
                this.elements.amountInputField.sendKeys(amount);
                this.elements.submitButton.click();
                this.elements.currentBalance.getText().then((afterBalance) => {
                    expect(afterBalance * 1).toEqual(beforeBalance * 1 + amount);
                });
            });

        });
    },

    // W I T H D R A W //

    makeWithdrawTransaction (amount) {
        this.elements.withdrawButton.click().then(() => {
            this.elements.amountInputField.sendKeys(amount);
            this.elements.submitButton.click();
        });
    },

    makeWithdrawTransactionAndCheckBalance (amount) {
        this.getCurrentBalance().then((beforeBalance) => {
            this.makeWithdrawTransaction(amount);

            this.getCurrentBalance().then((afterBalance) => {
                expect(afterBalance * 1).toEqual(beforeBalance * 1 - amount);
            });
        });
    },

    // C O M M O N //

    checkTransactionResultMessage (expectedMessage) {
        expect(this.elements.resultMessage.getText()).toEqual(expectedMessage);
    },

    checkThatAccountIsClosed () {
        expect(this.elements.accountClosedMessage.getText()).toEqual('Please open an account with us.');
    },

    checkThatAccountIsOpenedWithChosenCurrency (currency) {
        expect(this.getCurrentCurrency()).toEqual(currency);
    },

    changeAccount () {
        this.elements.currentCurrency.getText().then((currentAccountCurrency) => {
            this.elements.selectAccount.click().then(() => {
                element(by.xpath('//option[@value="number:1002"]')).click();
                this.elements.currentCurrency.getText().then((newAccountCurrency) => {
                    expect(currentAccountCurrency).not.toBe(newAccountCurrency);
                })
            })

        });
    },

    getCurrentBalance () {
        return this.elements.currentBalance.getText();
    },

    getCurrentCurrency () {
        return this.elements.currentCurrency.getText();
    },

    logoutFromAccount () {
        this.elements.logoutButton.click().then(() => {
            loginScreen.checkThatScreenOpened();
        })
    }
};