module.exports = {

    url: 'http://www.way2automation.com/angularjs-protractor/banking/#/login',

    elements: {
        customerLoginButton: element(by.buttonText('Customer Login')),
        managerLoginButton: element(by.buttonText('Bank Manager Login')),
    },

    goToCustomerLoginScreen () {
        this.elements.customerLoginButton.click();
    },

    goToManagerLoginScreen () {
        this.elements.managerLoginButton.click();
    },

    checkThatScreenOpened () {
        expect(this.elements.customerLoginButton.isDisplayed()).toBe(true);
    }
};