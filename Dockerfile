FROM atlassian/default-image:2

RUN echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/chrome.list

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN set -x \
    && apt-get update \
    && apt-get install -y \
        google-chrome-stable

COPY ./ ./tests

WORKDIR ./tests

RUN npm install

RUN ./node_modules/protractor/bin/webdriver-manager update --gecko=false

#RUN set -d \
#    &&  --name volume \
#    &&  --mount source=myvol,target=/reports \
#      nginx:latest

#RUN mkdir ./node_modules/protractor/node_modules
#
#RUN  mkdir ./node_modules/protractor/node_modules/webdriver-manager
#
#RUN cp -rf ./node_modules/webdriver-manager/selenium ./node_modules/protractor/node_modules/webdriver-manager/

ENTRYPOINT npm test